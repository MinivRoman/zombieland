import Zombieland from './App';
import './styles/index.css';
import { viewIntro } from './services/notification.service';

const rootElem = <HTMLDivElement>document.getElementById('root');
const zombieland = new Zombieland(rootElem, { width: 960, height: 480 });

viewIntro(zombieland.render.bind(zombieland));
