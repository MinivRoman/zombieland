import { IPosition } from '../GameObject';
import Person, { Behavior } from './Person';
import PlayerSprite from '../../sprites/player.sprite';
import playerSpriteUrl from '../../assets/sprites/player.png';
import Bullet from '../Bullet';

interface IPressedInputDevice {
  pressed: string | number | null;
  validKeys: string[] | number[];
};

export enum DirectionPlayer { Up, Left, Down, Right };

export default class Player extends Person {
  private sprite: PlayerSprite;
  private direction: DirectionPlayer;
  private keyboard: IPressedInputDevice;
  private mouse: IPressedInputDevice;
  public bulletList: Bullet[];

  public constructor(canvas: HTMLCanvasElement) {
    super();
    this.sprite = new PlayerSprite(playerSpriteUrl);
    this.size = {
      width: this.sprite.unit.width,
      height: this.sprite.unit.height
    };

    Person.canvas = canvas;

    this.velocity = 4;
    this.position = {
      x: (Person.canvas.width - this.sprite.unit.width) / 2,
      y: (Person.canvas.height - this.sprite.unit.height) / 2,
    };

    this.direction = DirectionPlayer.Down;
    
    this.keyboard = {
      pressed: null,
      validKeys: ['ArrowUp', 'ArrowRight', 'ArrowDown', 'ArrowLeft']
    };
    this.mouse = {
      pressed: null,
      validKeys: [0]
    };

    this.bulletList = [];

    this.setUserEvents();
  }
  public render(ctx: CanvasRenderingContext2D) {
    this.sprite.draw(ctx, this.position, this.direction, this.behavior);
    this.move();
    this.bulletList.forEach(bullet => {
      bullet.render(ctx);
    });
  }
  private move() {
    if (this.keyboard.pressed) {
      this.behavior = Behavior.Move;

      switch (this.keyboard.pressed) {
        case 'ArrowUp':
          this.direction = DirectionPlayer.Up;
          this.position.y += -this.velocity;
          break;
        case 'ArrowRight':
          this.direction = DirectionPlayer.Right;
          this.position.x += this.velocity;
          break;
        case 'ArrowDown':
          this.direction = DirectionPlayer.Down;
          this.position.y += this.velocity;
          break;
        case 'ArrowLeft':
          this.direction = DirectionPlayer.Left;
          this.position.x += -this.velocity;
          break;
      }
    }
  }
  private setUserEvents() {
    document.addEventListener('keydown', this.keyDownHandler.bind(this), false);
    document.addEventListener('keyup', this.keyUpHandler.bind(this), false);
    Person.canvas.addEventListener('mousedown', this.mouseDownHandler.bind(this), false);
    Person.canvas.addEventListener('mouseup', this.mouseUpHandler.bind(this), false);
  }
  private keyDownHandler(e: KeyboardEvent) {
    const { code } = e;
    if ((<string[]>this.keyboard.validKeys).includes(code)) {
      this.keyboard.pressed = code;
    }
  }
  private keyUpHandler(e: KeyboardEvent) {
    this.keyboard.pressed = null;
    this.behavior = Behavior.Stop;
  }
  private mouseDownHandler(e: MouseEvent) {
    const { button } = e;
    if ((<number[]>this.mouse.validKeys).includes(button)) {
      this.mouse.pressed = button;
      this.attack(this.getMousePositionOnCanvas(e));
    }
  }
  private mouseUpHandler(e: MouseEvent) {
    this.mouse.pressed = null;
  }
  private getMousePositionOnCanvas(e: MouseEvent): IPosition {
    return {
      x: e.clientX - Player.canvas.offsetLeft,
      y: e.clientY - Player.canvas.offsetTop
    };
  }
  private attack(mousePosition: IPosition) {
    this.createBullet(mousePosition);
  }
  private createBullet(mousePosition: IPosition) {
    const bullet: Bullet = new Bullet({ start: this.getPositionCenter(), end: mousePosition });
    this.bulletList.push(bullet);
  }
}
