import GameObject, { IPosition, ISize } from '../GameObject';

export enum Behavior { Stop, Move };

export default abstract class Person extends GameObject {
  static canvas: HTMLCanvasElement;

  public behavior: Behavior;
  protected velocity: number;
  
  public constructor(behavior: Behavior = Behavior.Stop, position?: IPosition, velocity: number = 0, size?: ISize) {
    super(position, size);

    this.behavior = behavior;
    this.velocity = velocity;
  }
  
  public abstract render(ctx: CanvasRenderingContext2D, payload?: object): void;
};
