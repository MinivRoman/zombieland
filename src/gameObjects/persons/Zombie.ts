import GameObject, { IPosition } from '../GameObject';
import Person, { Behavior } from './Person';
import ZombieSprite from '../../sprites/zombie.sprite';
import zombieSpriteUrl from '../../assets/sprites/zombie.png';
import Trajectory from '../../services/trajectory.service';

export enum DirectionZombie { Down, Left, Right, Up };

export default class Zombie extends Person {
  private sprite: ZombieSprite;
  private direction: DirectionZombie;

  public constructor(spriteStart?: IPosition, position?: IPosition) {
    super();
    
    this.sprite = new ZombieSprite(zombieSpriteUrl, spriteStart);
    this.size = {
      width: this.sprite.unit.width,
      height: this.sprite.unit.height
    };
    
    if (position) {
      this.position = position;
    }
    this.velocity = 1;
    this.direction = DirectionZombie.Down;
  }
  public render(ctx: CanvasRenderingContext2D, gameObject: GameObject) {
    this.sprite.draw(ctx, this.position, this.direction, this.behavior);
    this.moveTo(gameObject);
  }
  private moveTo(gameObject: GameObject) {
    this.behavior = Behavior.Move;

    const { step, direction } = Trajectory.calculateHypotenuse(
      this.getPositionCenter(),
      gameObject.getPositionCenter()
    );

    if (
      this.getPositionCenter().y >= gameObject.position.y &&
      this.getPositionCenter().y + this.size.height <= gameObject.position.y + gameObject.size.height
    ) {
      this.direction = direction.x == -1 ? DirectionZombie.Left : DirectionZombie.Right;
    } else {
      this.direction = direction.y == -1 ? DirectionZombie.Up : DirectionZombie.Down;
    }

    this.position.x += step.x * this.velocity * direction.x;
    this.position.y += step.y * this.velocity * direction.y;    
  }
}
