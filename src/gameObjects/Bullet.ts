import GameObject, { IPosition } from './GameObject';
import { loadImage } from '../helpers/image.helper';
import bulletImageUrl from '../assets/images/bullet.png';
import Trajectory from '../services/trajectory.service';

interface IPositionMovement {
  start: IPosition;
  end: IPosition;
}
interface IMovementTrajectory {
  step: IPosition;
  direction: IPosition;
}

export default class Bullet extends GameObject {
  private bulletImage: HTMLImageElement;
  private positionMovement: IPositionMovement;
  private movementTrajectory: IMovementTrajectory;
  private velocity: number;

  public constructor(positionMovement: IPositionMovement) {
    super();

    this.bulletImage = loadImage(bulletImageUrl);
    this.size = {
      width: this.bulletImage.width,
      height: this.bulletImage.height
    };
    this.positionMovement = this.correctPositionMovement(positionMovement);
    this.position = {
      x: positionMovement.start.x,
      y: positionMovement.start.y
    };
    this.movementTrajectory = Trajectory.calculateHypotenuse(
      this.positionMovement.start,
      this.positionMovement.end
    );
    this.velocity = 8;
  }
  private correctPositionMovement({ start, end }: IPositionMovement): IPositionMovement {
    const bulletCenterEnd: IPosition = {
      x: end.x - this.size.width / 2,
      y: end.y - this.size.height / 2
    };

    return {
      start,
      end: bulletCenterEnd
    };
  }
  public render(ctx: CanvasRenderingContext2D) {
    this.draw(ctx);
    this.move();
  }
  private draw(ctx: CanvasRenderingContext2D) {
    ctx.drawImage(this.bulletImage, this.position.x, this.position.y);
  }
  private move() {
    const { step, direction } = this.movementTrajectory;

    this.position.x += step.x * this.velocity * direction.x;
    this.position.y += step.y * this.velocity * direction.y;
  }
}
