export interface IPosition {
  x: number;
  y: number;
}
export interface ISize {
  width: number;
  height: number;
}

export default abstract class GameObject {
  public position: IPosition;
  public size: ISize;

  public constructor(position: IPosition = { x: 0, y: 0 }, size: ISize = { width: 0, height: 0 }) {
    this.position = position;
    this.size = size;
  }

  public getPositionCenter(): IPosition {
    return {
      x: this.position.x + this.size.width / 2,
      y: this.position.y + this.size.height / 2
    };
  }
}
