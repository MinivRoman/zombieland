import canvasBGUrl from './assets/images/canvasBackground.jpg';
import GameObject from './gameObjects/GameObject';
import Player from './gameObjects/persons/Player';
import Zombie from './gameObjects/persons/Zombie';
import Countdown from './services/countdown.service';
import { Behavior } from './gameObjects/persons/Person';
import { loadImage } from './helpers/image.helper';
import { getRandomZombie, getRandomPositionZombie } from './helpers/zombie.helper';
import { viewResult } from './services/notification.service';

export interface ICanvasOptions {
  width: number;
  height: number;
}
interface IGameInfo {
  zombieKilled: number;
}
interface IResult {
  winner: boolean;
}

enum GameState { Start, Stop };
enum CollisionWithCanvas { X, Y, None };

export default class Zombieland {
  public static isCollisionBetweenObjects(gameObject1: GameObject, gameObject2: GameObject) {
    return (
      gameObject1.position.x + gameObject1.size.width >= gameObject2.position.x &&
      gameObject1.position.x <= gameObject2.position.x + gameObject2.size.width &&
      gameObject1.position.y + gameObject1.size.height >= gameObject2.position.y &&
      gameObject1.position.y <= gameObject2.position.y + gameObject2.size.height
    );
  }

  private canvas: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D;
  private canvasBackground: HTMLImageElement;
  private gameState: GameState;
  private player: Player;
  private zombieList: Zombie[];
  private info: IGameInfo;
  private countdown: Countdown;
  private timerZombieSet: number;

  public constructor(container: HTMLDivElement, canvasOptions: ICanvasOptions, canvasBackgroundUrl: string = canvasBGUrl) {
    this.canvas = this.createCanvas(container, canvasOptions);
    this.ctx = <CanvasRenderingContext2D>this.canvas.getContext('2d');
    this.canvasBackground = loadImage(canvasBackgroundUrl);

    this.gameState = GameState.Start;

    this.player = new Player(this.canvas);

    this.zombieList = [];
    this.addZombieSet();
    this.timerZombieSet = window.setInterval(this.addZombieSet.bind(this), 1000 * 5);
    this.info = {
      zombieKilled: 0
    };

    this.countdown = new Countdown(59);
    this.countdown.start();
  }
  private createCanvas(container: HTMLDivElement, { width, height }: ICanvasOptions): HTMLCanvasElement {
    const canvas = document.createElement('canvas');

    canvas.setAttribute('id', 'zombieland');
    canvas.setAttribute('width', `${width}px`);
    canvas.setAttribute('height', `${height}px`);

    container.appendChild(canvas);

    return canvas;
  }
  private addZombieSet() {
    for (let i = 0; i < 10; i++) {
      this.zombieList.push(new Zombie(
        getRandomZombie(),
        getRandomPositionZombie(this.player.position, this.canvas)
      ));
    }
  }
  public render() {
    if (this.gameState == GameState.Start) {
      this.clearCanvas();
      this.setCanvasBackground();

      this.player.render(this.ctx);
      this.zombieList.forEach(zombie => {
        zombie.render(this.ctx, this.player);
      });

      this.collisionDetection();

      this.drawInfo();

      if (this.countdown.end) {
        this.setResult({ winner: true });
      }

      requestAnimationFrame(this.render.bind(this));
    }
  }
  private clearCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  private setCanvasBackground() {
    const pattern: CanvasPattern = <CanvasPattern>this.ctx.createPattern(this.canvasBackground, 'repeat');
    this.ctx.fillStyle = pattern;
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }
  private setResult({ winner }: IResult) {
    this.stop();

    const message: string = `You - ${winner ? 'win' : 'lost'}!`;
    viewResult(message, this.restart.bind(this));
  }
  stop() {
    this.gameState = GameState.Stop;
    this.countdown.stop();
    clearInterval(this.timerZombieSet);
  }
  restart() {
    this.reset();
    this.render();
  }
  reset() {
    this.gameState = GameState.Start;

    this.player = new Player(this.canvas);

    this.zombieList = [];
    this.addZombieSet();
    this.timerZombieSet = window.setInterval(this.addZombieSet.bind(this), 1000 * 5);
    this.info = {
      zombieKilled: 0
    };

    this.countdown = new Countdown(59);
    this.countdown.start();
  }
  private drawInfo() {
    this.ctx.save();
    this.ctx.font = 'status-bar';
    this.ctx.fillStyle = 'rgba(0, 255, 255, .8)';
    this.ctx.fillText(`Countdown: ${this.countdown.remains}`, 8, 20);
    this.ctx.fillText(`Killed: ${this.info.zombieKilled}`, 8, 36);
    this.ctx.restore();
  }
  private collisionDetection() {
    this.playerCollisionDetection();
    this.bulletCollisionDetection();
    this.zombieCollisionDetection();
  }
  private playerCollisionDetection() {
    const collisionPlayerWithCanvas: CollisionWithCanvas = this.isCollisionWithCanvas(this.player);

    if (this.player.behavior == Behavior.Move) {
      if (collisionPlayerWithCanvas == CollisionWithCanvas.X) {
        this.player.position.x = Math.min(
          Math.max(this.player.position.x, 0),
          this.canvas.width - this.player.size.width
        );
      } else if (collisionPlayerWithCanvas == CollisionWithCanvas.Y) {
        this.player.position.y = Math.min(
          Math.max(this.player.position.y, 0),
          this.canvas.height - this.player.size.height
        );
      }
    }
  }
  private bulletCollisionDetection() {
    this.player.bulletList = this.player.bulletList.filter(bullet => this.isCollisionWithCanvas(bullet) == CollisionWithCanvas.None);

    let bulletIndex: number = -1, zombieIndex: number = -1;
    this.player.bulletList.forEach((bullet, bulletI) => {
      this.zombieList.forEach((zombie, zombieI) => {
        if (Zombieland.isCollisionBetweenObjects(bullet, zombie)) {
          bulletIndex = bulletI;
          zombieIndex = zombieI;
        }
      });
    });
    if (bulletIndex != -1 && zombieIndex != -1) {
      this.info.zombieKilled++;
      this.player.bulletList.splice(bulletIndex, 1);
      this.zombieList.splice(zombieIndex, 1);
    }
  }
  private zombieCollisionDetection() {
    this.zombieList.forEach((zombie) => {
      if (Zombieland.isCollisionBetweenObjects(this.player, zombie)) {
        this.highlightObject(zombie);
        this.setResult({ winner: false });
      }
    });
  }
  private isCollisionWithCanvas(gameObject: GameObject): CollisionWithCanvas {
    if (gameObject.position.x < 0 || gameObject.position.x + gameObject.size.width > this.canvas.width) {
      return CollisionWithCanvas.X;
    } else if (gameObject.position.y < 0 || gameObject.position.y + gameObject.size.height > this.canvas.height) {
      return CollisionWithCanvas.Y;
    } else {
      return CollisionWithCanvas.None;
    }
  }
  private highlightObject(gameObject: GameObject) {
    const { position, size } = gameObject;

    this.ctx.save();
    this.ctx.fillStyle = 'rgba(255, 0, 0, .5)';
    this.ctx.fillRect(position.x, position.y, size.width, size.height);
    this.ctx.restore();
  }
};
