import { IPosition } from '../gameObjects/GameObject';
import Sprite, { ISpriteUnit } from './sprite';
import { Behavior } from '../gameObjects/persons/Person';
import { DirectionZombie } from '../gameObjects/persons/Zombie';
import { loadImage } from '../helpers/image.helper';

export default class ZombieSprite extends Sprite {
  private start: IPosition;
  
  public constructor(spriteImageUrl: string, spriteStart: IPosition = { x: 0, y: 0 }) {
    const unit: ISpriteUnit = { width: 32, height: 32, rows: 4, columns: 3 };
    
    super(unit);

    this.start = spriteStart;
    
    ZombieSprite.spriteImage = loadImage(spriteImageUrl);
  }
  public draw(ctx: CanvasRenderingContext2D, position: IPosition, direction: DirectionZombie, behavior: Behavior) {
    this.frame.row = direction;

    const sx = (this.start.x * this.unit.columns + this.frame.column) * this.unit.width;
    const sy = (this.start.y * this.unit.rows + this.frame.row) * this.unit.height;  

    ctx.drawImage(
      ZombieSprite.spriteImage,
      sx, sy, this.unit.width, this.unit.height,
      position.x, position.y, this.unit.width, this.unit.height
    );

    if (behavior == Behavior.Move) {
      this.animate();
    }
  }
  public animate() {
    if (++this.frame.counter == this.frame.delay) {
      if (this.frame.column < this.unit.columns - 1) {
        this.frame.column++;
      } else {
        this.frame.column = 0;
      }
      this.frame.counter = 0;
    }
  }
}
