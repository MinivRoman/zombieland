import Sprite, { ISpriteUnit } from './sprite';
import { IPosition } from '../gameObjects/GameObject';
import { DirectionPlayer } from '../gameObjects/persons/Player';
import { Behavior } from '../gameObjects/persons/Person';
import { loadImage } from '../helpers/image.helper';

export default class PlayerSprite extends Sprite {
  constructor(spriteImageUrl: string) {
    const unit: ISpriteUnit = { width: 64, height: 64, rows: 4, columns: 9 };

    super(unit);
    PlayerSprite.spriteImage = loadImage(spriteImageUrl);
  }
  public draw(ctx: CanvasRenderingContext2D, position: IPosition, direction: DirectionPlayer, behavior: Behavior) {
    const sx = behavior == Behavior.Stop ? 0 : (this.frame.column + 1) * this.unit.width;
    const sy = direction * this.unit.height;

    ctx.drawImage(
      PlayerSprite.spriteImage,
      sx, sy, this.unit.width, this.unit.height,
      position.x, position.y, this.unit.width, this.unit.height
    );

    if (behavior == Behavior.Move) {
      this.animate();
    }
  }
  public animate() {
    if (++this.frame.counter == this.frame.delay) {
      if (this.frame.column < this.unit.columns - 2) {
        this.frame.column++;
      } else {
        this.frame.column = 0;
      }
      this.frame.counter = 0;
    }
  }
}
