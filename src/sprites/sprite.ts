import { IPosition } from '../gameObjects/GameObject';
import { Behavior } from '../gameObjects/persons/Person';
import { DirectionPlayer } from '../gameObjects/persons/Player';
import { DirectionZombie } from '../gameObjects/persons/Zombie';

export interface ISpriteUnit {
  width: number;
  height: number;
  rows: number;
  columns: number;
}
export interface ISpriteFrame {
  row: number;
  column: number;
  delay: number;
  counter: number;
}

type Direction = DirectionPlayer | DirectionZombie;

export default abstract class Sprite {
  static spriteImage: HTMLImageElement;
  
  public unit: ISpriteUnit;
  protected frame: ISpriteFrame;

  public constructor(unit: ISpriteUnit, frame: ISpriteFrame = { row: 0, column: 0, delay: 4, counter: 0 }) {
    this.unit = unit;
    this.frame = frame;
  }

  public abstract draw(ctx: CanvasRenderingContext2D, position: IPosition, direction: Direction, behavior?: Behavior): void;
  public abstract animate(): void;
}
