import { getRandomIntInclusive } from './random.helper';
import { IPosition } from '../gameObjects/GameObject';

export function getRandomZombie(): IPosition {
  return {
    x: getRandomIntInclusive(0, 3),
    y: getRandomIntInclusive(0, 1)
  };
};
export function getRandomPositionZombie(playerPosition: IPosition, canvas: HTMLCanvasElement, safeZone: number = 200): IPosition {
  const position: IPosition = { x: 0, y: 0 };

  const randomLeftPositionX = getRandomIntInclusive(0, playerPosition.x - safeZone);
  const randomRightPositionX = getRandomIntInclusive(playerPosition.x + safeZone, canvas.width);
  if (playerPosition.x - safeZone <= 0) {
    position.x = randomRightPositionX;
  }
  else if (playerPosition.x + safeZone >= canvas.width) {
    position.x = randomLeftPositionX;
  }
  else {
    const random = getRandomIntInclusive(0, 1);
    if (random) {
      position.x = randomRightPositionX;
    } else {
      position.x = randomLeftPositionX;
    }
  }

  const randomLeftPositionY = getRandomIntInclusive(0, playerPosition.y - safeZone);
  const randomRightPositionY = getRandomIntInclusive(playerPosition.y + safeZone, canvas.height);
  if (playerPosition.y - safeZone <= 0) {
    position.y = randomRightPositionY;
  }
  else if (playerPosition.y + safeZone >= canvas.height) {
    position.y = randomLeftPositionY;
  }
  else {
    const random = getRandomIntInclusive(0, 1);
    if (random) {
      position.y = randomRightPositionY;
    } else {
      position.y = randomLeftPositionY;
    }
  }

  return position;
};
