export function loadImage(imageSrc: string, callback?: VoidFunction): HTMLImageElement {
  const img = new Image();
  img.src = imageSrc;

  callback && img.addEventListener('load', callback, false);

  return img;
} 
