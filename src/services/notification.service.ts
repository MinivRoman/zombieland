import Swal from 'sweetalert2'
import './notification.css';

export function viewIntro(callback: Function): void {
  Swal.fire({
    title: 'Zombieland',
    html: `
      <table>
        <caption>Controls</caption>
        <thead>
          <tr>
            <th>Action</th>
            <th>Key</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Up</td>
            <td>&uarr;</td>
          </tr>
          <tr>
            <td>Right</td>
            <td>&rarr;</td>
          </tr>
          <tr>
            <td>Down</td>
            <td>&darr;</td>
          </tr>
          <tr>
            <td>Left</td>
            <td>&larr;</td>
          </tr>
          <tr>
            <td>Fire</td>
            <td>left &#128432; button</td>
          </tr>
        </tbody>
      </table>
      <p class='slogan'>fight or die</p>
    `,
    allowOutsideClick: false,
    allowEscapeKey: false
  }).then(result => {
    if (result) {
      callback();
    }
  });
}

export function viewResult(message: string, callback: Function): void {
  Swal.fire({
    icon: 'info',
    title: message,
    allowOutsideClick: false,
    allowEscapeKey: false,
    confirmButtonText: 'Restart',
  }).then(result => {
    if (result) {
      callback();
    }
  });
}
