import { IPosition } from '../gameObjects/GameObject';

interface ITrajectoryHypotenuse  {
  step: IPosition;
  direction: IPosition;
}

export default class Trajectory {
  public static calculateHypotenuse(start: IPosition, end: IPosition): ITrajectoryHypotenuse {
    const direction = {
      x: start.x < end.x ? 1 : -1,
      y: start.y < end.y ? 1 : -1
    };

    const deltaDistance = {
      x: Math.abs(start.x - end.x),
      y: Math.abs(start.y - end.y)
    };

    const step = { x: 0, y: 0 };
    if (deltaDistance.x > deltaDistance.y) {
      step.x = 1;
      step.y = deltaDistance.y / deltaDistance.x;
    } else {
      step.y = 1;
      step.x = deltaDistance.x / deltaDistance.y;
    }

    return { step, direction };
  }
}
