export default class Countdown {
  private totalSeconds: number;
  private timerId: number;
  public remains: string;
  public end: boolean;

  public constructor(totalSeconds: number) {
    this.totalSeconds = totalSeconds;
    this.timerId = -1;
    this.remains = '00:00';
    this.end = false;
  }
  public start() {
    this.setTime();

    this.timerId = window.setInterval(() => {
      this.setTime();

      if (!this.totalSeconds--) {
        this.end = true;
        this.stop();
        return;
      }
    }, 1000);
  }
  private setTime() {
    let minutes: number | string = 0, seconds: number | string = 0;

    seconds = this.totalSeconds % 60;
    minutes = Math.floor(this.totalSeconds / 60) % 60;

    seconds = seconds < 10 ? "0" + seconds : seconds;
    minutes = minutes < 10 ? "0" + minutes : minutes;

    this.remains = `${minutes}:${seconds}`;
  }
  public stop() {
    clearInterval(this.timerId);
  }
}
